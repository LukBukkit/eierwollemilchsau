package tk.lukbukkit.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.lukbukkit.EierWolleMilchSau;

public class ItemsInv implements CommandExecutor{
    EierWolleMilchSau plugin;
    
    public ItemsInv(EierWolleMilchSau plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if(!(cs instanceof Player)){
            cs.sendMessage("Du musst ein Spieler sein!");
        }
        Player p = (Player) cs;
        p.openInventory(plugin.ItemInv);
        
        return true;
    }
    
}
