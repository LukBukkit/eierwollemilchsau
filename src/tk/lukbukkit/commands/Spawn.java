package tk.lukbukkit.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;

public class Spawn implements CommandExecutor{
    
    tk.lukbukkit.EierWolleMilchSau plugin;
    
    public Spawn(tk.lukbukkit.EierWolleMilchSau plugin) {
        this.plugin = plugin;
    }
    
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if (!(cs instanceof Player)) {
            cs.sendMessage("Du bist kein Spieler!");
            return true;
        }
        Player p = (Player) cs;
        
        Pig pig = (Pig) p.getLocation().getWorld().spawnEntity(p.getLocation(), EntityType.PIG);
        
        pig.setCustomName(ChatColor.GOLD + "Eierlegende Wollemilchsau");
        pig.setCustomNameVisible(true);
        
        p.sendMessage(ChatColor.GREEN + "Das Schwein wurde gespawnt!");
        
        plugin.enPig.add(pig);
        
        
        return true;
    }
    
}
