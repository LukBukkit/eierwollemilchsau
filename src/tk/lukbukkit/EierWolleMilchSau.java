package tk.lukbukkit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Pig;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class EierWolleMilchSau extends JavaPlugin{
    
    File f = new File("plugins/AllesSau", "pigs.yml");
    FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
    
    @Override
    public void onDisable() {
        
        //Schweine speichern
        for(Pig p : this.enPig){
            this.IDPig.add(p.getEntityId());
        }
        
        cfg.set("pigs", this.IDPig);
        
        //Bukkit.broadcastMessage(this.UUIDPig.toString());
        
        try {
            cfg.save(f);
        } catch (IOException ex) {
            System.out.println("[EierWolleMilchSau] Fehler beim Speichern!");
        }
        
        //ArrayList für Inventar leeren
        this.ItemInv.clear();
        
        System.out.println("[EierWolleMilchSau] Plugin gestoppt!");
    }
    
    public Inventory ItemInv;
    
    @Override
    public void onEnable() {
        
        //Commands
        this.getCommand("Allessau").setExecutor(new tk.lukbukkit.commands.Spawn(this));
        this.getCommand("Allessauitems").setExecutor(new tk.lukbukkit.commands.ItemsInv(this));
        
        //Events
        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new tk.lukbukkit.events.PlayerInteract(this), this);
        pm.registerEvents(new tk.lukbukkit.events.EntityDeath(this), this);
        ///pm.registerEvents(new tk.lukbukkit.events.InventoryClickListener(this), this);
        
        //Eier
        Random r = new Random();
        int rv = r.nextInt(150);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new EggRunnable(this), 0L, rv*20L);
        
        //Schweine laden
        for(World w : this.getServer().getWorlds())
            {
                for(Pig p : w.getEntitiesByClass(Pig.class))
                {
                    if(cfg.getIntegerList("pigs").contains(p.getEntityId()))
                    {
                        this.enPig.add(p);
                    }
                }
            }
        
        //Rezept für Schwein
        ItemStack SpawnPig0 = tk.lukbukkit.Items.SuperKarrotte();
        
        ShapelessRecipe recipe0 = new ShapelessRecipe(SpawnPig0);
        
        recipe0.addIngredient(1, Material.DIAMOND);
        recipe0.addIngredient(2, Material.WOOL);
        recipe0.addIngredient(2, Material.MILK_BUCKET);
        recipe0.addIngredient(2, Material.EGG);
        
        Bukkit.addRecipe(recipe0);
        
        //Rezept für Schuhe
        ItemStack LeichteFeder1 = tk.lukbukkit.Items.LeichteFeder();

        ItemStack CloudBoots2 = tk.lukbukkit.Items.CloudBoots();
        
        ShapedRecipe recipe1 = new ShapedRecipe(CloudBoots2);
        
        recipe1.shape("000", "F0F", "FBF");
        
        recipe1.setIngredient('B', Material.LEATHER_BOOTS);
        recipe1.setIngredient('F', LeichteFeder1.getData());
        
        Bukkit.addRecipe(recipe1);
        
        //Inventar "ItemsInv"
        
        
        ItemInv = Bukkit.createInventory(null, 9, ChatColor.GOLD + "[AllesSau] Alle Items");
        
        
        
        ItemInv.setItem(0, SpawnPig0);
        ItemInv.setItem(1, LeichteFeder1);
        ItemInv.setItem(2, CloudBoots2);
        
        
        
        
        pm.registerEvents(new tk.lukbukkit.events.InventoryClickListener(this), this);
        
        System.out.println("[EierWolleMilchSau] Plugin gestartet!");
    }
    
    
    public HashMap<Integer, Integer> pig = new HashMap<>();
    public ArrayList<Pig> enPig = new ArrayList<>();
    public ArrayList<Integer> IDPig = new ArrayList<>();
}
