package tk.lukbukkit;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class Items {
    
    public static ItemStack SuperKarrotte() {
        ItemStack SpawnPig0 = new ItemStack(Material.CARROT_ITEM, 1);
        ItemMeta meta0 = SpawnPig0.getItemMeta();
        meta0.setDisplayName(ChatColor.GOLD + "Super Karrotte");
        List<String> itemLore0 = new ArrayList<String>();
        itemLore0.add("Mit diesem Item kannst");
        itemLore0.add("du ein Schwein in eine");
        itemLore0.add("Eierlegende WolleMilchSau");
        itemLore0.add("verwandeln!");
        meta0.setLore(itemLore0);
        meta0.addEnchant(Enchantment.SILK_TOUCH, 5, true);
        SpawnPig0.setItemMeta(meta0);

        return SpawnPig0;
}
    public static ItemStack LeichteFeder() {
        ItemStack LeichteFeder1 = new ItemStack(Material.FEATHER, 1);
        ItemMeta meta1 = LeichteFeder1.getItemMeta();
        meta1.addEnchant(Enchantment.PROTECTION_FALL, 5, true);
        meta1.setDisplayName(ChatColor.GOLD + "Leichte Feder");
        LeichteFeder1.setItemMeta(meta1);
        
        return LeichteFeder1;
    }
    
    public static ItemStack CloudBoots() {
        ItemStack CloudBoots2 = new ItemStack(Material.LEATHER_BOOTS, 1);
        ItemMeta meta = CloudBoots2.getItemMeta();
        meta.addEnchant(Enchantment.PROTECTION_FALL, 5, true);
        meta.setDisplayName(ChatColor.WHITE + "Cloud Boots");
        CloudBoots2.setItemMeta(meta);
        LeatherArmorMeta lameta = (LeatherArmorMeta) CloudBoots2.getItemMeta();
        lameta.setColor(Color.WHITE);
        CloudBoots2.setItemMeta(lameta);
        
        return CloudBoots2;
    }
    
}
