package tk.lukbukkit;

import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Pig;
import org.bukkit.inventory.ItemStack;

public class EggRunnable implements Runnable{
    tk.lukbukkit.EierWolleMilchSau plugin;
    
    EggRunnable(tk.lukbukkit.EierWolleMilchSau plugin){
        this.plugin = plugin;
    }
    
    public Pig tp;
    
    @Override
    public void run() {
        
        if(plugin.enPig.isEmpty()){
            System.out.println("Keine Schweine vorhanden!");
        }
        
        
                
        for(Pig p : plugin.enPig) {
            Bukkit.broadcastMessage(p.getCustomName() + p.toString() + p.getEntityId() + p.getVehicle() + "1");
            if(p.isValid()) {
                tp = p;
                Bukkit.broadcastMessage(p.getCustomName() + p.toString() + p.getEntityId() + p.getVehicle() + "1");
                Random r = new Random();
                int ri = r.nextInt(5);
                if(ri == 1){
                    Bukkit.broadcastMessage(p.getCustomName() + p.toString() + p.getEntityId() + p.getVehicle() + "1");
                    int rt = r.nextInt(10);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        @Override
                        public void run() {
                            //Bukkit.broadcastMessage(tp.getCustomName() + tp.toString() + tp.getEntityId() + tp.getVehicle() + "1");
                            tp.getWorld().dropItemNaturally(tp.getLocation(), new ItemStack (Material.EGG, 1));
                            tp.getWorld().playSound(tp.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
                        }
                    }, rt*20L);
                }
            } else {
                plugin.enPig.remove(p);
            }
        }
    }
    
}
