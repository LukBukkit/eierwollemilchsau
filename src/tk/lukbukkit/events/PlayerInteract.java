package tk.lukbukkit.events;

import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class PlayerInteract implements Listener{
    tk.lukbukkit.EierWolleMilchSau plugin;

    public PlayerInteract(tk.lukbukkit.EierWolleMilchSau plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onSign(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        if(e.getRightClicked() != null){
            Entity en = e.getRightClicked();
            if(en instanceof Pig) {
                Pig pig = (Pig) en;
                if(plugin.enPig.contains(pig)){
                    PlayerInventory pi = p.getInventory();

                    //Eimer
                    if(p.getItemInHand().getType() == (Material.BUCKET)) {
                        int buckets = pi.getItemInHand().getAmount();
                        ItemStack iStack = p.getItemInHand();
                        //pi.remove(p.getItemInHand());
                        buckets = buckets - 1;
                        int currentslot = pi.getHeldItemSlot(); 
                        if(buckets != 0) {
                           pi.setItem(currentslot, new ItemStack(Material.BUCKET, buckets));
                           pi.addItem(new ItemStack(Material.MILK_BUCKET, 1));
                        } else if (buckets == 0){
                            pi.remove(pi.getItemInHand());
                            pi.addItem(new ItemStack(Material.MILK_BUCKET, 1));
                        }
                        p.updateInventory();
                    }

                    //Schere
                    if(p.getItemInHand().getType() == (Material.SHEARS)) {
                        if(!(plugin.pig.containsKey(en.getEntityId())) || !(Bukkit.getScheduler().isQueued(plugin.pig.get(en.getEntityId())))) {
                            Random r = new Random();
                            int rd = r.nextInt(3) + 1;
                            ItemStack wool = new ItemStack (Material.WOOL, rd);
                            en.getWorld().dropItemNaturally(en.getLocation(), wool);
                            en.getWorld().playSound(en.getLocation(), Sound.SHEEP_SHEAR, 1, 1);
                            plugin.pig.put(en.getEntityId(), Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                                @Override
                                public void run () {

                                }
                            }, r.nextInt(600)*20L));
                        } else {
                            p.sendMessage(ChatColor.RED + "Du musst noch etwas warten!");
                        }
                    }
                }
                //Item
                if(p.getItemInHand().getType().equals(Material.CARROT_ITEM) && p.getItemInHand().getEnchantmentLevel(Enchantment.SILK_TOUCH) == 5 && p.getItemInHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Super Karrotte")){
                    ItemStack PlayerItemHand = p.getItemInHand();
                    PlayerInventory pi = p.getInventory();
                    int HandItemAm = PlayerItemHand.getAmount();
                    if(HandItemAm > 1){
                        PlayerItemHand.setAmount(HandItemAm - 1);
                    } else {
                        pi.remove(PlayerItemHand);
                    }
                    
                    final Location pigloc = pig.getLocation();
                    pig.remove();
                    Pig spig = (Pig) pigloc.getWorld().spawnEntity(pigloc, EntityType.PIG);
        
                    spig.setCustomName(ChatColor.GOLD + "Eierlegende Wollemilchsau");
                    spig.setCustomNameVisible(true);
        
                    p.sendMessage(ChatColor.GREEN + "Du hast ein Schwein gespawnt!");
        
                    plugin.enPig.add(spig);
                    
                }
            }
        }
    }
}
