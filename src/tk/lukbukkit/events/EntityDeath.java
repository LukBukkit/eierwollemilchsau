package tk.lukbukkit.events;

import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Pig;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tk.lukbukkit.EierWolleMilchSau;

public class EntityDeath implements Listener{
    tk.lukbukkit.EierWolleMilchSau plugin;

    public EntityDeath(EierWolleMilchSau plugin) {
        this.plugin = plugin;
    }
    
    
    @EventHandler
    public void onEntityDeath(EntityDeathEvent e){
        Entity en = e.getEntity();
        if(en instanceof Pig){
            Pig pig = (Pig) en;
            if(plugin.enPig.contains(pig)){
                List<ItemStack> drops = e.getDrops();
                drops.clear();
                Random r = new Random();
                Random r2 = new Random();
                
                ItemStack pig0 = new ItemStack(Material.GRILLED_PORK, (r.nextInt(3) + 1));
                ItemMeta meta0 = pig0.getItemMeta();
                meta0.setDisplayName(ChatColor.GOLD + "Golden Porkchop");
                pig0.setItemMeta(meta0);
                
                drops.add(pig0);
                
                if(r.nextInt(19) == 1){
                    drops.add(tk.lukbukkit.Items.SuperKarrotte());
                }
                
                if(r2.nextInt(19) == 1){
                    drops.add(tk.lukbukkit.Items.LeichteFeder());
                }
                
                
                plugin.enPig.remove(pig);
            }
        }
    }
    
}
