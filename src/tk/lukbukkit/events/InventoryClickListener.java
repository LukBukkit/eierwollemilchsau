package tk.lukbukkit.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import tk.lukbukkit.Items;

public class InventoryClickListener implements Listener{
    tk.lukbukkit.EierWolleMilchSau plugin;
    
    public InventoryClickListener(tk.lukbukkit.EierWolleMilchSau plugin){
        this.plugin = plugin;
    }
    
    
    @EventHandler
    public void onClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        
        if(e.getInventory().equals(plugin.ItemInv)){
            p.updateInventory();
            e.setCancelled(true);
            ItemStack cItem = e.getCurrentItem();
            
            if(cItem.equals(tk.lukbukkit.Items.CloudBoots())){
                p.getInventory().addItem(tk.lukbukkit.Items.CloudBoots());
            }
            
            if(cItem.equals(tk.lukbukkit.Items.LeichteFeder())){
                p.getInventory().addItem(tk.lukbukkit.Items.LeichteFeder());
            }
            
            if(cItem.equals(Items.SuperKarrotte())){
                p.getInventory().addItem(Items.SuperKarrotte());
            }
        }
        
        
    }
}
